# Assignment 2 Instructions

Assignment 2

All tasks below must be done in the index.html file. There is a script tag available near the bottom of the page which you can use. 

Task: Write a function called tagUpdater which updates the element's content as provided in one of the arguments of the function. This function will take three arguments: selectorType (can be tag, class name, or CSS selector), selector (this is the value to be used for selecting within the required selector type) and finally the content which will be updated. (10 marks)

Task: Once above function is written:
                Call the function which replaces the Lorem Ipsum text found in each of the Feature Boxes with the Text 'Welcome to feature #', where # is the number of the box within the HTML (5 marks)